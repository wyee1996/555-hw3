package edu.upenn.cis.stormlite.mapreduce;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.upenn.cis455.mapreduce.worker.StorageFactory;
import edu.upenn.cis455.mapreduce.worker.StorageInterface;

public class TestGetDatabaseInstance {

	@Test
	public void test() {
		StorageInterface db = StorageFactory.getDatabaseInstance("./storage/node1", false);
		assertTrue(db != null);
	}

}
