package edu.upenn.cis.stormlite.mapreduce;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.upenn.cis.stormlite.Config;
import edu.upenn.cis455.mapreduce.worker.WorkerServer;

public class TestStartupWorker {

	@Test
	public void test() {
		try {
			Config config = new Config();
			config.put("workerIndex", "0");
			WorkerServer.createWorker(config);
		} catch(RuntimeException ex) {
			assertTrue(true);
		}
	}
	
	@Test
	public void test2() {
		try {
			Config config = new Config();
			config.put("workerIndex", "0");
			config.put("workerList", "[127.0.0.1:8001]");
			WorkerServer.createWorker(config);
			assertTrue(true);
		} catch(RuntimeException ex) {
			assertFalse(false);
		}
	}

}
