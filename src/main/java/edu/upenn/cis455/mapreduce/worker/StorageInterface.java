package edu.upenn.cis455.mapreduce.worker;

import java.util.ArrayList;

import com.sleepycat.je.Environment;
import com.sleepycat.persist.EntityStore;

public interface StorageInterface {

	public EntityStore getEntityStore();
	
	public Environment getEnvironment();
	
	public void addNewItem(String key, String value);
	
	public ArrayList<ItemDAO> getAllItems();
	
	public void deleteAll(ArrayList<ItemDAO> items);
     
    /**
     * Shuts down / flushes / closes the storage system
     */
    public void close();
}
