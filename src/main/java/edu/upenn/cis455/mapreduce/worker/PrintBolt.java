package edu.upenn.cis455.mapreduce.worker;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.IRichBolt;
import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.distributed.ConsensusTracker;
import edu.upenn.cis.stormlite.routers.StreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;

/**
 * A trivial bolt that simply outputs its input stream to the
 * console
 * 
 * @author zives
 *
 */
public class PrintBolt implements IRichBolt {
	static Logger log = LogManager.getLogger(PrintBolt.class);
	
	Fields myFields = new Fields();
	
	String outputDir;

    /**
     * To make it easier to debug: we have a unique ID for each
     * instance of the PrintBolt, aka each "executor"
     */
    String executorId = UUID.randomUUID().toString();
	//String executorId = "PrintBolt"+WorkerServer.randomNum();

	@Override
	public void cleanup() {
		// Do nothing

	}

	@Override
	public boolean execute(Tuple input) {
		log.info("Processing "+input.toString());
		String key = input.getStringByField("key");
		String value = input.getStringByField("value");
		if (!input.isEndOfStream()) {
			//System.out.println(getExecutorId() + ": " + input.toString());
			try {
				File dir = new File(outputDir);
				dir.mkdir();
				File newFile = new File(dir,"output.txt");
				if (newFile.createNewFile()) {
					log.info("Created new output.txt file in folder "+outputDir);
				}
				FileWriter writer = new FileWriter(outputDir+"/"+"output.txt", true);
				BufferedWriter bw = new BufferedWriter(writer);
				bw.write("("+key+","+value+")");
				bw.newLine();
				bw.close();
				WorkerServer.addToResults("("+key+","+value+")");
				
			}catch (IOException e) {
				log.error("Error occurred creating outputting results to output.txt in folder "+outputDir);
				e.printStackTrace();
			}
			
			
		}
		return true;
	}

	@Override
	public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
		if (!stormConf.containsKey("outputDirectory") || !stormConf.containsKey("storageDirectory")) {
			throw new RuntimeException("PrintBolt doesn't know where to output results!");
		}
		outputDir = stormConf.get("storageDirectory")+"/"+stormConf.get("outputDirectory");
	}

	@Override
	public String getExecutorId() {
		return executorId;
	}

	@Override
	public void setRouter(StreamRouter router) {
		// Do nothing
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(myFields);
	}

	@Override
	public Fields getSchema() {
		return myFields;
	}

}
