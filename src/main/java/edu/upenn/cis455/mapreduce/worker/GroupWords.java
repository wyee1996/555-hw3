package edu.upenn.cis455.mapreduce.worker;

import java.util.Iterator;

import edu.upenn.cis455.mapreduce.Context;
import edu.upenn.cis455.mapreduce.Job;

/**
 * Sample class to implement both mapper and reducer.
 * The reducer just returns the key as key and value.
 * 
 * @author ZacharyIves
 *
 */
public class GroupWords implements Job {
//
//	Integer one = 1;
//	
//	@Override
//	public void map(String key, String value, Context context, String sourceExecutor) {
//		context.write(value, one.toString(), sourceExecutor);
//		
//	}
//
	@Override
	public void reduce(String key, Iterator<String> values, Context context, String sourceExecutor) {
		Integer count = 0;
		while(values.hasNext()) {
			count++;
			values.next();
		}
		context.write(key, count.toString(),sourceExecutor);
		
	}
	@Override
	public void map(String key, String value, Context context, String sourceExecutor) {
		context.write(value, value, sourceExecutor);
	}
//
//	@Override
//	public void reduce(String key, Iterator<String> values, Context context, String sourceExecutor) {
//		context.write(key, key, sourceExecutor);
//	}

}
