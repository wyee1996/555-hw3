package edu.upenn.cis455.mapreduce.worker;

import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;

import com.sleepycat.je.DatabaseException;
import com.sleepycat.persist.EntityCursor;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;


public class ItemDataAccessor {

	// Session accessor
	private PrimaryIndex<String,ItemDAO> itemByKey;
	
	public ItemDataAccessor(EntityStore store) {
		// primary key for item class
		itemByKey = store.getPrimaryIndex(String.class, ItemDAO.class);
	}
	
	/**
	 * Utility function to create a new item and save it to the database, or add to it if already exists
	 * @return
	 */
	public void addNewItem(String key, String value) {
		
		ItemDAO existingItem = itemByKey.get(key);
		if (existingItem != null) {
			existingItem.addToValues(value);
			itemByKey.put(existingItem);
		}
		else {
			ItemDAO newItem = new ItemDAO(key, value);
			itemByKey.put(newItem);
		}
	}
	
	public void deleteAll(ArrayList<ItemDAO> items) {
		for (ItemDAO item : items) {
			itemByKey.delete(item.getKey());
		}
	}
	
	public ArrayList<ItemDAO> getAllItems(){
		ArrayList<ItemDAO> items = new ArrayList<ItemDAO>();
		EntityCursor<ItemDAO> itemCursor = itemByKey.entities();
		for (ItemDAO item : itemCursor) {
			items.add(item);
		}
		itemCursor.close();
		return items;
	}
	
}
