package edu.upenn.cis455.mapreduce.worker;

import java.io.File;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;

import static spark.Spark.*;

import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.StoreConfig;


public final class DatabaseEnvironment implements StorageInterface{

	public static Environment env;
	public static EntityStore store;
	public static ItemDataAccessor itemDataAccessor;
	
	public void setup(File homeDirectory, boolean readOnly) throws DatabaseException{
		
		EnvironmentConfig envConfig = new EnvironmentConfig();
		StoreConfig storeConfig = new StoreConfig();
		
		envConfig.setReadOnly(readOnly);
		storeConfig.setReadOnly(readOnly);
		
		// If environment opened for write, then we want to be able to
		// create the environment and entity store if they don't exist
		envConfig.setAllowCreate(true);
		storeConfig.setAllowCreate(true);
		
		envConfig.setTransactional(true);
		storeConfig.setTransactional(true);
		
		// open the environment and entity store
		env = new Environment(homeDirectory, envConfig);
		store = new EntityStore(env, "EntityStore", storeConfig);
		
		// open the Data Accessors
		itemDataAccessor = new ItemDataAccessor(store);
		
	}
	
	// Return a handle to the entire store
	@Override
	public EntityStore getEntityStore() {
		return store;
	}
	
	// Return a handle to the environment
	@Override
	public Environment getEnvironment() {
		return env;
	}
	
	// Close store and environment
	public void close() {
		if (store != null) {
			try {
				store.close();
			}
			catch(DatabaseException dbe) {
				System.err.println("Error closing store: "+dbe.toString());
				System.exit(-1);
			}
		}
		if (env != null) {
			try {
				env.close();
			}
			catch(DatabaseException dbe){
				System.err.println("Error closing environment: "+dbe.toString());
				System.exit(-1);
			}
		}
	}

	@Override
	public void addNewItem(String key, String value) {
		itemDataAccessor.addNewItem(key, value);
	}

	@Override
	public ArrayList<ItemDAO> getAllItems() {
		return itemDataAccessor.getAllItems();
	}

	@Override
	public void deleteAll(ArrayList<ItemDAO> items) {
		itemDataAccessor.deleteAll(items);
		
	}

}
