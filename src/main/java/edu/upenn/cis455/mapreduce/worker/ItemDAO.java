package edu.upenn.cis455.mapreduce.worker;

import java.util.ArrayList;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;


@Entity
public class ItemDAO {
	
	@PrimaryKey
	private String key;
	
	private ArrayList<String> values = new ArrayList<String>();
	
	public ItemDAO() {}
	
	public ItemDAO(String key, String value) {
		this.key = key;
		this.values.add(value);
	}
	
	public String getKey() {
		return this.key;
	}
	
	public void addToValues(String value) {
		this.values.add(value);
	}
	
		
	public ArrayList<String> getValues() {
		return this.values;
	}
		
}
