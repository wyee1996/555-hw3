package edu.upenn.cis455.mapreduce.worker;

import static spark.Spark.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upenn.cis.stormlite.Config;
import edu.upenn.cis.stormlite.DistributedCluster;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.distributed.WorkerHelper;
import edu.upenn.cis.stormlite.distributed.WorkerJob;
import edu.upenn.cis.stormlite.routers.StreamRouter;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis455.mapreduce.RunJobRoute;
import spark.Spark;

/**
 * Simple listener for worker creation 
 * 
 * @author zives
 *
 */
public class WorkerServer {
    static final Logger log = LogManager.getLogger(WorkerServer.class);

    static DistributedCluster cluster = new DistributedCluster();

    List<TopologyContext> contexts = new ArrayList<>();

    static List<String> topologies = new ArrayList<>();
    
    private static Integer myPort;
    
    private static String masterHostname;
    
    private static String storageDir;
    
    public static StorageInterface db;
    
    private static Hashtable<String,String> workerStatus = new Hashtable<String, String>();
    
    private static Integer keysRead = 0;
    
    private static Integer keysWritten = 0;
    
    private static ArrayList<String> results = new ArrayList<String>();

    public WorkerServer(int myPort) throws MalformedURLException {

        log.info("Creating server listener at socket " + myPort);

        port(myPort);
        final ObjectMapper om = new ObjectMapper();
        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        Spark.post("/definejob", (req, res) -> {

            WorkerJob workerJob;
            try {
                workerJob = om.readValue(req.body(), WorkerJob.class);

                try {
                    log.info("Processing job definition request" + workerJob.getConfig().get("job") +
                            " on machine " + workerJob.getConfig().get("workerIndex"));
                    
                    // Add worker's storage directory to config
                    Config config = workerJob.getConfig();
                    config.put("storageDirectory", storageDir);
                    workerJob.setConfig(config);
                    
                    contexts.add(cluster.submitTopology(workerJob.getConfig().get("job"), workerJob.getConfig(), 
                            workerJob.getTopology(),db));

                    // Add a new topology
                    synchronized (topologies) {
                        topologies.add(workerJob.getConfig().get("job"));
                    }
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                res.status(200);
                return "Job launched";
            } catch (IOException e) {
                e.printStackTrace();

                // Internal server error
                res.status(500);
                return e.getMessage();
            } 

        });
        
        Spark.get("/shutdown", (req,res) -> {
        	while(true) {
        		System.out.println(workerStatus.get("status"));
        		if (workerStatus.get("status").equals("IDLE")) {
        			System.out.println("shutting down server");
        			shutdown();
        			System.exit(0);
        			return "successfully shutdown";
        		}
        	}
        });

        Spark.post("/runjob", new RunJobRoute(cluster));

        Spark.post("/pushdata/:stream", (req, res) -> {
            try {
                String stream = req.params(":stream");
                log.debug("Worker received: " + req.body());
                Tuple tuple = om.readValue(req.body(), Tuple.class);

                log.debug("Worker received: " + tuple + " for " + stream);

                // Find the destination stream and route to it
                StreamRouter router = cluster.getStreamRouter(stream);

                if (contexts.isEmpty())
                    log.error("No topology context -- were we initialized??");

                TopologyContext ourContext = contexts.get(contexts.size() - 1);

                // Instrumentation for tracking progress
                if (!tuple.isEndOfStream())
                    ourContext.incSendOutputs(router.getKey(tuple.getValues()));

                // TODO: handle tuple vs end of stream for our *local nodes only*
                // Please look at StreamRouter and its methods (execute, executeEndOfStream, executeLocally, executeEndOfStreamLocally)
                if (tuple.isEndOfStream()) {
                	router.executeEndOfStreamLocally(ourContext, tuple.getSourceExecutor());
                }
                else if (!tuple.isEndOfStream()) {
                	router.executeLocally(tuple, ourContext, tuple.getSourceExecutor());
                }
                
                
                return "OK";
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

                res.status(500);
                return e.getMessage();
            }

        });

    }

    public static void createWorker(Map<String, String> config) {
        if (!config.containsKey("workerList"))
            throw new RuntimeException("Worker spout doesn't have list of worker IP addresses/ports");

        if (!config.containsKey("workerIndex"))
            throw new RuntimeException("Worker spout doesn't know its worker ID");
        else {
            String[] addresses = WorkerHelper.getWorkers(config);
            String myAddress = addresses[Integer.valueOf(config.get("workerIndex"))];

            log.debug("Initializing worker " + myAddress);

            URL url;
            try {
                url = new URL(myAddress);

                new WorkerServer(url.getPort());
            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public static void shutdown() {
        synchronized(topologies) {
            for (String topo: topologies)
                cluster.killTopology(topo);
        }

        cluster.shutdown();
    }

    /**
     * Simple launch for worker server.  Note that you may want to change / replace
     * most of this.
     * 
     * @param args
     * @throws MalformedURLException
     */
    public static void main(String args[]) throws MalformedURLException {
        if (args.length < 3) {
            System.out.println("Usage: WorkerServer [port number] [master host/IP]:[master port] [storage directory]");
            System.exit(1);
        }

        myPort = Integer.valueOf(args[0]);
        masterHostname = args[1];
        storageDir = args[2];
        
        System.out.println("Worker node startup, on port " + myPort);

        new WorkerServer(myPort);
        
        storageDir = "./" + storageDir;
        
        db = StorageFactory.getDatabaseInstance(storageDir, false);
        log.info("Retrieved database instance");
        
        // Set worker status starting off
        setWorkerStatus("IDLE", "None");
        
        while(true) {
        	try {
				issueWorkerStatus();
				Thread.sleep(10000);
			} catch (IOException | InterruptedException e) {
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e1) {
				}
			}
        }
    }
    
//    public static String randomNum() {
//    	String nums = "0123456789";
//		StringBuilder sb = new StringBuilder();
//		for (int i = 0; i < 5; i++) {
//			int idx = (int)(nums.length()*Math.random());
//			sb.append(nums.charAt(idx));
//		}
//		return sb.toString();
//    }
    
    public static void updateJob(String job) {
    	workerStatus.replace("job", job);
    }
    
    public static void addKeysRead() {
    	keysRead = keysRead++;
    }
    
    public static void flushKeysRead() {
    	keysRead = 0;
    }
    
    public static void addKeysWritten() {
    	keysWritten = keysWritten++;
    }
    
    public static void addKeysWritten(Integer keys) {
    	keysWritten += keys;
    }
    
    public static void flushKeysWritten() {
    	keysWritten = 0;
    }
    
    public static void updateStatus(String status) {
    	workerStatus.replace("status", status);
    }
    
    // Appends new results outputted from PrintBolt
    public static void addToResults(String newResults) {
    	results.add(newResults);
    	//System.out.println("Adding new results: "+newResults);
    }
    
    // Used to get results into a string format for output on /status screen for each worker
    public static String getResults() {
    	StringBuilder sb = new StringBuilder();
    	sb.append("[");
    	for (int i=0; i< results.size(); i++) {
    		if (i >= 100) break;
    		if (i != 0) {
    			sb.append(",");
    		}
    		sb.append(results.get(i));
    	}
    	sb.append("]");
    	return sb.toString();
    }
    
    public static void setWorkerStatus(String status, String job) {
    	workerStatus.put("status", status);
    	workerStatus.put("job", job);
    }
    
    private static void issueWorkerStatus() throws IOException {
    	StringBuilder sb = new StringBuilder();
    	sb.append("http://");
    	sb.append(masterHostname);
    	sb.append("/workerstatus?port=");
    	sb.append(myPort.toString());
    	sb.append("&status="+workerStatus.get("status"));
    	sb.append("&job="+workerStatus.get("job"));
    	sb.append("&keysRead="+keysRead.toString());
    	sb.append("&keysWritten="+keysWritten.toString());
    	sb.append("&results="+getResults());
		URL url = new URL(sb.toString());
		
		System.out.println("Sending worker status to " + url.toString());
		
		HttpURLConnection conn = (HttpURLConnection)url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Host",masterHostname);
		
		conn.setRequestProperty("Content-Type", "text/plain");
		
		conn.connect();
		
		BufferedReader in = new BufferedReader(
                new InputStreamReader(conn.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

		
    }
    
}
