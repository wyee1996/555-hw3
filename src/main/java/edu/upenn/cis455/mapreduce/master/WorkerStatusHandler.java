package edu.upenn.cis455.mapreduce.master;

import java.net.URI;
import java.net.URL;
import java.util.Hashtable;import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis455.mapreduce.worker.WorkerServer;
import spark.Request;
import spark.Response;
import spark.Route;

public class WorkerStatusHandler implements Route{
	
	static final Logger log = LogManager.getLogger(WorkerStatusHandler.class);

	@Override
	public Object handle(Request request, Response response) throws Exception {
		
		System.out.println("Received worker update");
		
		String ipAddress = request.ip();
		String port = request.queryParams("port");
		String ipPortComb = ipAddress+":"+port;
		Integer workerNum;
		if (!MasterServer.workerPortIpMap.contains(ipPortComb)) {
			MasterServer.workerPortIpMap.add(ipPortComb);
			workerNum = MasterServer.workerPortIpMap.indexOf(ipPortComb);
			log.info("Registering worker at "+ipPortComb+" as Worker #"+workerNum);
		}
		else {
			workerNum = MasterServer.workerPortIpMap.indexOf(ipPortComb);
		}
		
		String status = request.queryParams("status");
		String job = request.queryParams("job");
		Integer keysRead = Integer.parseInt(request.queryParams("keysRead"));
		Integer keysWritten = Integer.parseInt(request.queryParams("keysWritten"));
		String results = request.queryParams("results");
		WorkerStatus workerStatus = new WorkerStatus(ipAddress, port, status, job, keysRead, keysWritten, results);
		if (MasterServer.workerStatuses.size() > workerNum) {
			MasterServer.workerStatuses.set(workerNum, workerStatus);
			
		}else {
			MasterServer.workerStatuses.add(workerStatus);
		}
		
		return "success";
	}

}
