package edu.upenn.cis455.mapreduce.master;

import static spark.Spark.*;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Hashtable;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;

import edu.upenn.cis.stormlite.Config;
import edu.upenn.cis455.mapreduce.RunJobRoute;

public class MasterServer {  
	
	public static ArrayList<WorkerStatus> workerStatuses = new ArrayList<WorkerStatus>();
	public static ArrayList<String> workerPortIpMap = new ArrayList<String>();
	
    public static void registerStatusPage() {
        get("/status", new StatusPageHandler());
        post("/submitjob", new SubmitJobHandler());
    }
    
    public static void registerWorkerStatusReports() {
    	get("/workerstatus", new WorkerStatusHandler());
    	get("/shutdown", new ShutdownHandler());
    }

    /**
     * The mainline for launching a MapReduce Master.  This should
     * handle at least the status and workerstatus routes, and optionally
     * initialize a worker as well.
     * 
     * @param args
     */
    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("Usage: MasterServer [port number]");
            System.exit(1);
        }

        int myPort = Integer.valueOf(args[0]);
        port(myPort);

        System.out.println("Master node startup, on port " + myPort);

        // TODO: you may want to adapt parts of edu.upenn.cis.stormlite.mapreduce.TestMapReduce here

        registerStatusPage();
        //post("/runjob",)

        // TODO: route handler for /workerstatus reports from the workers
        
        registerWorkerStatusReports();
    }
    
}

