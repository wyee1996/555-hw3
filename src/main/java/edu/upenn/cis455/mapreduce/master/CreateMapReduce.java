package edu.upenn.cis455.mapreduce.master;

import edu.upenn.cis.stormlite.Config;
import edu.upenn.cis.stormlite.Topology;
import edu.upenn.cis.stormlite.TopologyBuilder;
import edu.upenn.cis.stormlite.bolt.MapBolt;
import edu.upenn.cis.stormlite.bolt.ReduceBolt;
import edu.upenn.cis.stormlite.distributed.WorkerJob;
import edu.upenn.cis.stormlite.spout.FileSpout;

public class CreateMapReduce {

	private static final String WORD_SPOUT = "WORD_SPOUT";
    private static final String MAP_BOLT = "MAP_BOLT";
    private static final String REDUCE_BOLT = "REDUCE_BOLT";
    private static final String PRINT_BOLT = "PRINT_BOLT";
	
	public CreateMapReduce(Config config) {
		
		Topology topology = CreateTopology(config);
		WorkerJob workerJob = new WorkerJob(topology, config);
		
	}
	
	private Topology CreateTopology(Config config) {
		TopologyBuilder builder = new TopologyBuilder();
		
//		FileSpout spout = new WordFileSpout();
//        MapBolt bolt = new MapBolt();
//        ReduceBolt bolt2 = new ReduceBolt();
//        PrintBolt printer = new PrintBolt();
//		
//		builder.setSpout(WORD_SPOUT, spout, parallelism);
//		
		Topology topology = builder.createTopology();
		
		return topology;
	}
}
