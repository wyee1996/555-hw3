package edu.upenn.cis455.mapreduce.master;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import spark.Request;
import spark.Response;
import spark.Route;

public class ShutdownHandler implements Route{

	@Override
	public Object handle(Request request, Response response) throws Exception {
		shutdownWorkers();
		System.exit(0);
		return "shutting down";
	}
	
	private static void shutdownWorkers() {
		
		for (String worker : MasterServer.workerPortIpMap) {
			try {
				StringBuilder sb = new StringBuilder();
		    	sb.append("http://");
		    	sb.append(worker);
		    	sb.append("/shutdown");
				URL url = new URL(sb.toString());
				
				System.out.println("Sending shutdown request to " + url.toString());
				
				HttpURLConnection conn = (HttpURLConnection)url.openConnection();
				conn.setDoOutput(true);
				conn.setRequestMethod("GET");
				conn.setRequestProperty("Host",worker);
				
				conn.setRequestProperty("Content-Type", "text/plain");
				
				conn.connect();
				
				BufferedReader in = new BufferedReader(
		                new InputStreamReader(conn.getInputStream()));
		        String inputLine;
		        StringBuffer response = new StringBuffer();
		        conn.disconnect();
			} catch(IOException e) {}
			
		}
		
    	

//        while ((inputLine = in.readLine()) != null) {
//            System.out.println(inputLine);
//        }
		
//		OutputStream os = conn.getOutputStream();
//		byte[] toSend = parameters.getBytes();
//		os.write(toSend);
//		os.flush();
		
    }

}
