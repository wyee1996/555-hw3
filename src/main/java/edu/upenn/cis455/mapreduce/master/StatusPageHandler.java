package edu.upenn.cis455.mapreduce.master;

import java.util.Set;

import spark.Request;
import spark.Response;
import spark.Route;

public class StatusPageHandler implements Route{

	@Override
	public Object handle(Request request, Response response) throws Exception {
		response.type("text/html");
		
		String webform = "<form method=\"POST\" action=\"/submitjob\" id=\"form1\">\n" + 
				" Job Name: <input type=\"text\" name=\"jobname\"/><br/>\n" + 
				" Class Name: <input type=\"text\" name=\"classname\"/><br/>\n" + 
				" Input Directory: <input type=\"text\" name=\"input\"/><br/>\n" + 
				" Output Directory: <input type=\"text\" name=\"output\"/><br/>\n" + 
				" Map Threads: <input type=\"text\" name=\"map\"/><br/>\n" + 
				" Reduce Threads: <input type=\"text\" name=\"reduce\"/><br/>\n" + 
				"</form>\n";
		
		String submit = "<button type=\"submit\" form=\"form1\" value=\"Submit\">Submit</button>";

        return ("<html><head><title>Master</title></head>\n" +
                "<body><p>"+getWorkerStatus()+"</p><p>"+webform+submit+"</p></body></html>");
	}
	
	private String getWorkerStatus() {
		StringBuilder sb = new StringBuilder();
		for (int i=0; i<MasterServer.workerStatuses.size();i++) {
			WorkerStatus workerStatus = MasterServer.workerStatuses.get(i);
			sb.append("<div>");
			sb.append(i+": ");
			sb.append("port="+workerStatus.port+", ");
			sb.append("status="+workerStatus.status+", ");
			sb.append("job="+workerStatus.job+", ");
			sb.append("keysRead="+workerStatus.keysRead.toString()+", ");
			sb.append("keysWritten="+workerStatus.keysWritten.toString()+", ");
			sb.append("results="+workerStatus.results.toString());
			sb.append("</div>");
		}
		return sb.toString();
	}

}
