package edu.upenn.cis455.mapreduce.master;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upenn.cis.stormlite.Config;
import edu.upenn.cis.stormlite.Topology;
import edu.upenn.cis.stormlite.TopologyBuilder;
import edu.upenn.cis.stormlite.bolt.MapBolt;
import edu.upenn.cis.stormlite.bolt.ReduceBolt;
import edu.upenn.cis.stormlite.distributed.WorkerHelper;
import edu.upenn.cis.stormlite.distributed.WorkerJob;
import edu.upenn.cis.stormlite.spout.FileSpout;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis455.mapreduce.worker.PrintBolt;
import edu.upenn.cis455.mapreduce.worker.WordFileSpout;
import spark.Request;
import spark.Response;
import spark.Route;

public class SubmitJobHandler implements Route{
	
	static Logger log = LogManager.getLogger(SubmitJobHandler.class);

	private static final String WORD_SPOUT = "WORD_SPOUT";
    private static final String MAP_BOLT = "MAP_BOLT";
    private static final String REDUCE_BOLT = "REDUCE_BOLT";
    private static final String PRINT_BOLT = "PRINT_BOLT";
	
	@Override
	public Object handle(Request request, Response response) throws Exception {
		
		// set up config
		Config config = createConfig(request);
		
		// Create the topology
		Topology topology = createTopology(config);
		
		// Create the new worker job object to send
		WorkerJob workerJob = new WorkerJob(topology, config);
		
		// Send job to workers
		submitToWorkers(config, workerJob);
		
		
		return "submitted job";
	}
	
	// submits jobs to workers
	private void submitToWorkers(Config config, WorkerJob job) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
        mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
		try {
			String[] workers = WorkerHelper.getWorkers(config);

			int i = 0;
			for (String dest: workers) {
		        config.put("workerIndex", String.valueOf(i++));
				if (sendJob(dest, "POST", config, "definejob", 
						mapper.writerWithDefaultPrettyPrinter().writeValueAsString(job)).getResponseCode() != 
						HttpURLConnection.HTTP_OK) {
					throw new RuntimeException("Job definition request failed");
				}
			}
			for (String dest: workers) {
				if (sendJob(dest, "POST", config, "runjob", "").getResponseCode() != 
						HttpURLConnection.HTTP_OK) {
					throw new RuntimeException("Job execution request failed");
				}
			}
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
	        System.exit(0);
		}       
	}
	
	private Topology createTopology(Config config) {
		FileSpout spout = new WordFileSpout();
        MapBolt bolt = new MapBolt();
        ReduceBolt bolt2 = new ReduceBolt();
        PrintBolt printer = new PrintBolt();

        TopologyBuilder builder = new TopologyBuilder();

        // Only one source ("spout") for the words
        builder.setSpout(WORD_SPOUT, spout, 1);
        
        // Parallel mappers, each of which gets specific words
        builder.setBolt(MAP_BOLT, bolt, Integer.valueOf(config.get("mapExecutors"))).fieldsGrouping(WORD_SPOUT, new Fields("value"));
        //builder.setBolt(MAP_BOLT, bolt, Integer.valueOf(config.get("mapExecutors"))).shuffleGrouping(WORD_SPOUT);
        
        // Parallel reducers, each of which gets specific words
        builder.setBolt(REDUCE_BOLT, bolt2, Integer.valueOf(config.get("reduceExecutors"))).fieldsGrouping(MAP_BOLT, new Fields("key"));

        // Only use the first printer bolt for reducing to a single point
        builder.setBolt(PRINT_BOLT, printer, 1).firstGrouping(REDUCE_BOLT);
        
        Topology topo = builder.createTopology();
        
        return topo;
	}

	private Config createConfig(Request request) {
		String jobName = request.queryParams("jobname");
		String className = request.queryParams("classname");
		String input = request.queryParams("input");
		String output = request.queryParams("output");
		String numMaps = request.queryParams("map");
		String numReduce = request.queryParams("reduce");
		
		Config config = new Config();
		config.put("job", jobName);
		config.put("mapClass", className);
		config.put("reduceClass", className);
		config.put("inputDirectory", input);
		config.put("outputDirectory", output);
		config.put("spoutExecutors", "1");
		config.put("mapExecutors",numMaps);
		config.put("reduceExecutors", numReduce);
		config.put("workerList", constructWorkerList(MasterServer.workerPortIpMap));
		
		return config;
	}
	
	// Constructs a string of the worker list to add to the config
	public String constructWorkerList(ArrayList<String> workers) {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		int length = workers.size();
		
		for (int i=0; i < length; i++) {
			sb.append(workers.get(i));
			
			if (i < length-1) {
				sb.append(",");
			}
		}
		sb.append("]");
		return sb.toString();
	}
	
	private HttpURLConnection sendJob(String dest, String reqType, Config config, String job, String parameters) throws IOException {
		URL url = new URL(dest + "/" + job);
		
		log.info("Sending request to " + url.toString());
		
		HttpURLConnection conn = (HttpURLConnection)url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod(reqType);
		
		if (reqType.equals("POST")) {
			conn.setRequestProperty("Content-Type", "application/json");
			
			OutputStream os = conn.getOutputStream();
			byte[] toSend = parameters.getBytes();
			os.write(toSend);
			os.flush();
		} else
			conn.getOutputStream();
		
		return conn;
    }
}

