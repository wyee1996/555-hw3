package edu.upenn.cis455.mapreduce.master;

import java.util.Hashtable;

public class WorkerStatus {

	public String ipAddress;
	public String port;
	public String status;
	public String job;
	public Integer keysRead;
	public Integer keysWritten;
	public String results;
	
	public WorkerStatus(String ipAddress, String port, String status, String job, Integer keysRead, Integer keysWritten, String results) {
		this.ipAddress = ipAddress;
		this.port = port;
		this.status = status;
		this.job = job;
		this.keysRead = keysRead;
		this.keysWritten = keysWritten;
		this.results = results;
	}
	
	
}
